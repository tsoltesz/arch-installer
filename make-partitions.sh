#!/bin/bash

SYSTEM_DISK=/dev/nvme.. #TODO
HOME_DISK=/dev/sda

echo "Creating the partitions..."
parted -s ${SYSTEM_DISK} mklabel gpt
parted -s ${SYSTEM_DISK} mkpart primary fat32 1MiB 550MiB #boot partition
parted -s ${SYSTEM_DISK} set 1 esp on
parted -s ${SYSTEM_DISK} mkpart primary ext4 551MiB 100% # / partition
parted -s ${HOME_DISK} mklabel gpt
parted -s ${HOME_DISK} mkpart primary ext4 0% 100% # /home partition

echo "Creating the filesystem on each partition..."
mkfs.fat -F32 ${SYSTEM_DISK}p1
mkfs.ext4 ${SYSTEM_DISK}p2
mkfs.ext4 ${HOME_DISK}

echo "Partitioning [DONE]"

