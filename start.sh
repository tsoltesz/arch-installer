#!/bin/bash

#main entry point for the installer

#partitioning
sh make-partitions.sh

#base system, pacstrap
sh make-base-system.sh

#arch-chroot into /mnt
sh make-arch-chroot.sh # TODO: this requires params. ask them at the beginning of the script!

#done - umount /mnt
sh umount-mnt.sh

echo "The base system is now ready. Please reboot the machine and run the /make-usable-system.sh script to follow the installation process."
echo "Note: It is possible that a hard swith-off is required now"
echo "See ya' later"
