#!/bin/bash

HOSTNAME=${1}
USERNAME=${2}
USERPASS=${3}

#arch-chroot into /mnt and run the scripts
arch-chroot /mnt ./arch-chroot-commands.sh "${HOSTNAME}" "${USERNAME}" "${USERPASS}" 
