#!/bin/bash

pacman --noconfirm -S git

git clone https://tsoltesz@bitbucket.org/tsoltesz/arch-installer.git installer
sh installer/start.sh
