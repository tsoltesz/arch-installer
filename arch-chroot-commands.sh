#!/bin/bash

HOSTNAME="${1}"
USERNAME="${2}"
USERPASS="${3}"
HOME_DIR="/home/${USERNAME}"

swapsize=$(cat /proc/meminfo | grep MemTotal | awk '{ print $2 }')
swapsize=$(($swapsize/1000))"MiB"

echo "Installing grub"
pacman --noconfirm -S grub os-prober mtools fuse2 efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
pacman --noconfirm -S intel-ucode
grub-mkconfig -o /boot/grub/grub.cfg

pacman --noconfirm -S openssh ntp

systemctl enable sshd.service
systemctl enable dhcpcd.service
systemctl enable ntpd.service
echo "$HOSTNAME" > /etc/hostname

echo 'root ALL=(ALL) ALL' > /etc/sudoers
echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers

#user creation
echo "Creating the 'commonsoda' user with a password of 'soda'"
useradd -m -G wheel -s /bin/bash commonsoda
echo "commonsoda:soda" | chpasswd 
if [ ! -z "${USERNAME}" ]; then
	echo "Creating ${USERNAME} user with a password of ${USERPASS}"
	useradd -m -G wheel -s /bin/bash "${USERNAME}"
	echo "${USERNAME}:${USERPASS}" | chpasswd
fi
echo "Changing 'root' password to 'code'"
echo "root:code" | chpasswd

# adjust your timezone here
ln -f -s /usr/share/zoneinfo/Europe/Budapest /etc/localtime
hwclock --systohc

echo en_US.UTF-8 UTF-8 > /etc/locale.gen
echo hu_HU.UTF-8 UTF-8 >> /etc/locale.gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
locale-gen

echo "creating swap file"
fallocate -l "${swapsize}" /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo /swapfile none swap defaults 0 0 >> /etc/fstab
