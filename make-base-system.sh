#!/bin/bash

echo "Clock synchronization"
timedatectl set-ntp true

root_partition=/dev/nvme... #TODO
boot_partition=/dev/nvme... #TODO
home_partition=/dev/sda1


echo "Mounting the partitions"
mount "${root_partition}" /mnt
mkdir -p /mnt/boot/efi
mkdir /mnt/home
mount "${boot_partition}" /mnt/boot/efi
mount "${home_partition}" /mnt/home

echo "Installing the base system"
echo "Server = http://ftp.energia.mta.hu/pub/mirrors/ftp.archlinux.org/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist
echo "Server = http://archmirror.hbit.sztaki.hu/archlinux/\$repo/os/\$arch" >> /etc/pacman.d/mirrorlist

pacman -Syy
pacstrap /mnt base base-devel

echo "Generating fstab"
genfstab -U /mnt > /mnt/etc/fstab
echo "FSTAB: "
cat /mnt/etc/fstab

echo "Base system creation is [DONE]. Please check the fstab"
